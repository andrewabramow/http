﻿/*
Реализуйте программу по осуществлению различных типов
HTTP-запросов к сервису httpbin.org.

Пользователь взаимодействует с программой с помощью
команд: «get», «post», «put», «delete», «patch». В 
зависимости от команды к серверу httpbin.org,
осуществляется запрос того или иного типа.
Содержимое ответа сервера выводится в стандартный
вывод.

Программа завершается, когда пользователь вводит
команду «ext».
*/

#include <iostream>
#include <cpr/cpr.h>
#include "http.h"

int main()
{
	std::cout << "Please input command" << std::endl;
	std::string cmd;
	while (cmd != "ext") {
		std::cin >> cmd;
		if (cmd == "get") {
			cpr::Response r = cpr::Get(cpr::Url("http://httpbin.org/get"));
			std::cout << r.text << std::endl;
		} 
		else if (cmd == "post") {
			cpr::Response r = cpr::Post(cpr::Url("http://httpbin.org/post"));
			std::cout << r.text << std::endl;
		}
		else if (cmd == "put") {
			cpr::Response r = cpr::Put(cpr::Url("http://httpbin.org/put"));
			std::cout << r.text << std::endl;
		}
		else if (cmd == "delete") {
			cpr::Response r = cpr::Delete(cpr::Url("http://httpbin.org/delete"));
			std::cout << r.text << std::endl;
		}
		else if (cmd == "patch") {
			cpr::Response r = cpr::Patch(cpr::Url("http://httpbin.org/patch"));
			std::cout << r.text << std::endl;
		}
	}
	return 0;
}

